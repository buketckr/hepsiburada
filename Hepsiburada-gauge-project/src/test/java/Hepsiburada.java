import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hepsiburada {
    WebDriver driver;

    @BeforeScenario
    public void hazirlik() {
        System.out.println("senaryo basladı");
        System.setProperty("webdriver.chrome.driver", "properties/driver/chromedriverhb.exe");
        this.driver = new ChromeDriver();

    }

    @Step("hepsiburada anasayfasına git")
    public void anasayfayagidilir() {
        driver.get("https://www.hepsiburada.com/");
        System.out.println("hepsiburada anasayfasına gidildi");
    }

    @Step("2 saniye bekle")
    public void anasayfabekleme() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    @Step("Giris yap butonu elementi üzerinde bekle")
    public void girisbekleme() throws InterruptedException {
        driver.findElement(By.cssSelector("span.sf-OldMyAccount-PhY-T")).click();
        Thread.sleep(1000);

    }

    @Step("<girisyap> butonuna tikla")
    public void girisYapTikla(String girisyap) throws InterruptedException {
        driver.findElement(By.cssSelector("a#login")).click();
    }

    @Step("<email> alanini doldur")
    public void implementation2(String email) throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"txtUserName\"]")).sendKeys("buketcakir1991@gmail.com");
    }

    @Step("<sifre> alalini doldur")
    public void implementation1(String sifre) throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"txtPassword\"]")).sendKeys("B12345678");
        Thread.sleep(1000);
    }

    @Step("giris butonuna tikla")
    public void girisbutonutiklama() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"btnLogin\"]")).click();
        Thread.sleep(3000);
    }

    @Step("5 saniye bekle")
    public void saniyeBekle() throws InterruptedException {
        Thread.sleep(5000);

    }

    @Step("Hesabım sayfasına git")
    public void hesabagit() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("div#myAccount")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"myAccount\"]/div/div[2]/ul/li[3]/a")).click();
    }

    @AfterScenario
    public void senaryoyusonlandir() {
        System.out.println("senaryo sonlandı");
    }

    }





